package com.example.douyuclient.clinet;

import com.example.douyuclient.encoder.DouyuEncoder;
import com.example.douyuclient.handler.DoyuClientHandler;
import com.example.douyuclient.request.DouyuRequest;
import com.example.douyuclient.util.DyMessage;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoop;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.GenericFutureListener;
import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: DouyuClinet
 * @Author: huangzf
 * @Date: 2018/7/23 11:05
 * @Description:
 */
@Slf4j
public class DouyuClinet {

    private final String host;
    private final int port;
    private int rommId;
    private volatile Bootstrap b;
    private EventLoopGroup workerGroup;

    private Channel channel = null;

    public DouyuClinet(String host, int port, int rommId){
        this.host = host;
        this.port = port;
        this.rommId = rommId;
    }

    public void start(){
        workerGroup = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(workerGroup)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.SO_KEEPALIVE,true)
                //.remoteAddress(new InetSocketAddress(host, port))
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel ch)
                        throws Exception {
                        ch.pipeline()

                            //.addLast(new FixedLengthFrameDecoder(Integer.MAX_VALUE))
                            .addLast("encoder",new DouyuEncoder())
                            //.addLast("decoder",new DouyuDecoder())
                            .addLast(new IdleStateHandler(0,30,0,TimeUnit.SECONDS))
                            .addLast(new DelimiterBasedFrameDecoder(Integer.MAX_VALUE, Unpooled.copiedBuffer("\0".getBytes())))
                            .addLast(new DoyuClientHandler(host,port,rommId));
                    }
                });

            ChannelFuture f = b.connect(new InetSocketAddress(host, port));        //6iIn


            //断线重连
            f.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture channelFuture) throws Exception {
                    if (!channelFuture.isSuccess()) {
                        final EventLoop loop = channelFuture.channel().eventLoop();
                        loop.schedule(new Runnable() {
                            @Override
                            public void run() {
                                System.err.println("服务端链接不上，开始重连操作...");
                                start();
                            }
                        }, 1L, TimeUnit.SECONDS);
                    } else {
                        channel = channelFuture.channel();
                        login();
                        jionGroup();
                        System.err.println("服务端链接成功...");
                    }
                }
            });

            channel.closeFuture().addListener(new GenericFutureListener<ChannelFuture>() {

                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    //group.shutdownGracefully();

                    //setConnectionState(CONNECTION_STATES.notConnected);
                }

            });
        } catch (Exception e){

        }
    }

    public void login(){
        if(channel.isOpen()){
            channel.writeAndFlush(new DouyuRequest(DyMessage.getLoginRequestData(rommId)));
        }

    }

    public void jionGroup(){
        channel.writeAndFlush(new DouyuRequest(DyMessage.getJoinGroupRequest(rommId,-9999)));
    }

    public static void main(String[] args) {
        DouyuClinet douyuClinet = new DouyuClinet("openbarrage.douyutv.com", 8601,485503);
        douyuClinet.start();
//        douyuClinet.login();
//        douyuClinet.jionGroup();
    }

}
