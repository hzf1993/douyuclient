package com.example.douyuclient.handler;

import com.example.douyuclient.clinet.DouyuClinet;
import com.example.douyuclient.request.DouyuRequest;
import com.example.douyuclient.util.MsgView;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.EventLoop;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName: DoyuClientHandler
 * @Author: huangzf
 * @Date: 2018/7/23 11:27
 * @Description:
 */
public class DoyuClientHandler extends SimpleChannelInboundHandler<ByteBuf> {

    private final int rommId;
    private final String host;
    private  final int port;

    private DouyuClinet douyuClinet = null;

    public DoyuClientHandler(String host,int port,int rommId) {
        this.rommId = rommId;
        this.host = host;
        this.port = port;

        douyuClinet = new DouyuClinet(host,port,rommId);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

        ctx.fireChannelActive();


        Channel channel = ctx.pipeline().channel();
        System.out.println("hahahh====" + channel.isActive());
//        //登录
//        channel.writeAndFlush(DyMessage.getLoginRequestData(rommId));
//        //加入弹幕组
//        channel.writeAndFlush(DyMessage.getJoinGroupRequest(rommId,-9999));


    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx,
        ByteBuf in) throws UnsupportedEncodingException {
        if(in.readableBytes() < 12){
            return;
        }

        MsgView msgView = new MsgView(in.readBytes(in).toString(CharsetUtil.UTF_8));

        Map<String,Object> resultMap = msgView.getMessageList();
        if (resultMap.get("type").equals("chatmsg")){
            System.out.println(resultMap.get("nn") + ":" + resultMap.get("txt"));
        }else if(resultMap.get("type").equals("uenter")){
            System.out.println("欢迎【" + resultMap.get("nn") + "】进入直播间！");
        }

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx,
        Throwable cause) {
        System.err.println("远程主机关闭一个现有连接");//4
//        final EventLoop eventLoop = ctx.channel().eventLoop();
//        eventLoop.schedule(new Runnable() {
//            @Override
//            public void run() {
//                douyuClinet.start();
//            }
//        }, 1L, TimeUnit.SECONDS);
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt)
        throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            if (event.state().equals(IdleState.READER_IDLE)) {
                System.out.println("READER_IDLE");
                // 超时关闭channel
                //ctx.close();
            } else if (event.state().equals(IdleState.WRITER_IDLE)) {
                System.out.println("WRITER_IDLE");
                String s = "type@=pingreq/tick@=+"+System.currentTimeMillis()+"/\0";
                DouyuRequest request = new DouyuRequest(s);
                ctx.channel().writeAndFlush(request);
                System.out.println("心跳发送成功!");
            } else if (event.state().equals(IdleState.ALL_IDLE)) {
                System.out.println("ALL_IDLE");
                // 发送心跳
                // ctx.channel().write("ping\n");
            }
        }
        super.userEventTriggered(ctx, evt);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("断线了。。。。。。");
        //使用过程中断线重连
        final EventLoop eventLoop = ctx.channel().eventLoop();
        eventLoop.schedule(new Runnable() {
            @Override
            public void run() {
                douyuClinet.start();
            }
        }, 1, TimeUnit.SECONDS);

        ctx.fireChannelInactive();
    }
}
