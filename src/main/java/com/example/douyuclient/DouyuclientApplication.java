package com.example.douyuclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DouyuclientApplication {

    public static void main(String[] args) {
        SpringApplication.run(DouyuclientApplication.class, args);
    }
}
