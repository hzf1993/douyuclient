package com.example.douyuclient.decoder;

import com.example.douyuclient.request.DouyuRequest;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.util.CharsetUtil;
import java.util.List;

/**
 * @ClassName: DouyuDecoder
 * @Author: huangzf
 * @Date: 2018/7/23 11:34
 * @Description:
 */
public class DouyuDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf buf,
        List<Object> list) throws Exception {

       // System.out.println(buf.readBytes(buf));
        //buf.alloc().buffer(100000);

        if(buf.readableBytes() < 12){
            return;
        }

        while (buf.readableBytes() != 0){
            list.add(new DouyuRequest(buf.readBytes(buf).toString(CharsetUtil.UTF_8)));
        }

    }
}
