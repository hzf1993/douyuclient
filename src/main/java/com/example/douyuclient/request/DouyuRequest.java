package com.example.douyuclient.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName: DouyuRequest
 * @Author: huangzf
 * @Date: 2018/7/23 11:01
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DouyuRequest {

    private String message;

}
