package com.example.douyuclient.encoder;

import com.example.douyuclient.request.DouyuRequest;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @ClassName: DouyuDecoder
 * @Author: huangzf
 * @Date: 2018/7/23 11:26
 * @Description:
 */
public class DouyuEncoder extends MessageToByteEncoder<DouyuRequest> {

    @Override
    protected void encode(ChannelHandlerContext ctx, DouyuRequest msg,
        ByteBuf buf) throws Exception {


        buf = buf.alloc().buffer(8 + msg.getMessage().getBytes().length);
        buf.writeIntLE(msg.getMessage().toString().getBytes().length+8);
        buf.writeIntLE(msg.getMessage().toString().getBytes().length+8);
        buf.writeShortLE(689);
        buf.writeByte(0);
        buf.writeByte(0);
        buf.writeBytes(msg.getMessage().toString().getBytes());
        ctx.writeAndFlush(buf);

    }
}
